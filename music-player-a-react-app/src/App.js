import Home from "./components/Home";
import Navbar from "./components/Navbar";
import Music from "./components/Music";
import {BrowserRouter,Routes,Route} from "react-router-dom"

function App() {
  return (
    
    <div className="App">
      
      <header className="App-header">
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route path="/Music" element={<Music />}/>
           
        </Routes>
        </BrowserRouter>  
      </header>
      
    </div>
    
    
  );
}

export default App;
