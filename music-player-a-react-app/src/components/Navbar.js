import logo from "../assets/logo.png"
import "./NavbarStyle.css"
import {Component} from "react"
import {Link} from "react-router-dom"
export class Navbar extends Component {
    
    state = {clicked : false};

    handleClick = () =>{
        this.setState({clicked :
        !this.state.clicked})
    }

    render() {return(
        <>
            <nav>
                <Link to="/" ><img src={logo} alt="Music Logo" height="50"/></Link><span id="logo">Player</span>
                <div>
                <ul id="navbar"
                className={this.state.clicked ? "#navbar active" : "navbar"}>
                    <li>
                        <Link to="/" className="active">Home</Link>
                    </li>
                    <li>
                        <Link to="/Music">Music</Link>
                    </li>
                </ul>
                </div>
                <div id="mobile" onClick={this.handleClick}>
                    <i
                    id="bar" className={
                        this.state.clicked ? "fas fa-times" : "fas fa-bars"
                    }
                    ></i>
                </div>
            </nav>
        
        </>
    )
}
}
export default Navbar;