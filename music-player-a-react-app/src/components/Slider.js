import "react-responsive-carousel/lib/styles/carousel.min.css"; 
import { Carousel } from 'react-responsive-carousel';
import item1 from "../assets/item1.jpg";
import item2 from "../assets/item2.jpg";
import item3 from "../assets/item3.jpg";

export default function Slider(){
    return(
        <>
        <Carousel infiniteLoop autoPlay>
                <div>
                    <img src={item1} />
                </div>
                <div>
                    <img src={item2} />
                </div>
                <div>
                    <img src={item3} />

                </div>
            </Carousel>
        </>
    );
};