import { useState, useEffect } from "react";
import Player from "./Player";
import "./MusicStyle.css";


export default function Music(){
    const [songs] = useState([
        {
          title: "Faded",
          artist: "Alan Walker",
          img_src: "./music/alanWalkerImage.jpg",
          src: "./music/Faded(PaglaSongs).mp3",
        },
        {
          title: "Hips Don't Lie",
          artist: "Shakira",
          img_src: "./music/hipsDontLieImage.jpg",
          src: "./music/Hips Don't Lie - Various.mp3",
        },
        {
          title: "Beat It",
          artist: "Michael Jackson",
          img_src: "./music/beatItImage.jpeg",
          src: "./music/Michael Jackson - Beat It.mp3",
        }
      ]);

    const [currentSongIndex, setCurrentSongIndex] = useState(0);
    const [nextSongIndex, setNextSongIndex] = useState(0);

    useEffect(() => {
        setNextSongIndex(() => {
        if (currentSongIndex + 1 > songs.length - 1) {
            return 0;
        } else {
            return currentSongIndex + 1;
        }
        });
    }, [currentSongIndex, songs.length]);

    return(
        <>
        <Player
        currentSongIndex={currentSongIndex}
        setCurrentSongIndex={setCurrentSongIndex}
        nextSongIndex={nextSongIndex}
        songs={songs}
        />
        </>
    )
}